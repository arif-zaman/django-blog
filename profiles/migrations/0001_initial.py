# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('firstname', models.CharField(max_length=120, null=True, blank=True)),
                ('lastname', models.CharField(max_length=120, null=True, blank=True)),
                ('username', models.CharField(unique=True, max_length=120)),
                ('email', models.EmailField(unique=True, max_length=75)),
                ('propic', models.FileField(null=True, upload_to=b'Files/%Y/%m/%d', blank=True)),
                ('timestamp', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
            ],
            options={
                'ordering': ['username'],
            },
            bases=(models.Model,),
        ),
    ]
