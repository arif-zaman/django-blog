from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from models import Profile
from homes.models import Article
from homes.models import Article_Category
from django.contrib.auth.models import User
from forms import ProfileForm

def count_categoryPost(name):
	list1 = []
	list2 = []

	categoy = Article_Category.objects.all()

	for item in categoy:
		list1.append(item)

	for item in list1:
		count = Article.objects.filter(writer = name,category=item).count()
		list2.append(count)

	return dict(zip(list1,list2))

def profile(request,username=''):
	himself = False
	login = False
	session_name = ''

	if 'name' in request.session:
		session_name = request.session['name']
		login = True

		if username==session_name:
			himself = True

	try:
		pro = Profile.objects.get(username=username)
		user = User.objects.get(username=username)
	except:
		return HttpResponseRedirect('/testblog/home/')
			
	tag = count_categoryPost(username)
	articles = Article.objects.filter(writer = username,private=False, is_active=True)
	private = Article.objects.filter(writer = username,private=True, is_active=True)

	return render_to_response("profile.html", locals(), context_instance=RequestContext(request))
			
def edit_profile(request,username=''):

	addr = "/testblog/profile/"+username+"/"

	if 'name' in request.session:
		name = request.session['name']

		if request.POST:
			user = Profile.objects.get(username=name)
			user.firstname = request.POST.get("firstname", "")
			user.lastname = request.POST.get("lastname", "")
			user.email = request.POST.get("email", "")
			if request.FILES.get("propic"):
				user.propic = request.FILES.get("propic")
			user.save()

	return HttpResponseRedirect(addr)

def about(request):
	himself = False
	login = False
	session_name = ''

	if 'name' in request.session:
		session_name = request.session['name']
		login = True

	return render_to_response('about.html', locals())

def change_password(request):
	login = False
	session_name = ''
	alert = False
	success = False

	if request.POST:
		session_name = request.session['name']
		password1 = request.POST.get("password1","")
		password2 = request.POST.get("password2","")
		user = User.objects.get(username=session_name)

		if user.check_password(password1):
			user.set_password(password2)
			user.save()
			success = True
		else :
			alert = True

	if 'name' in request.session:
		session_name = request.session['name']
		login = True
		
		
		return render_to_response("change_password.html", locals(), context_instance=RequestContext(request))

	return HttpResponseRedirect('/testblog/signin/')
