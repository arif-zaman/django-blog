# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('homes', '0005_auto_20150201_0332'),
    ]

    operations = [
        migrations.AlterField(
            model_name='article_comment',
            name='comment',
            field=models.TextField(),
            preserve_default=True,
        ),
    ]
