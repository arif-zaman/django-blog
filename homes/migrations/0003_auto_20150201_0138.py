# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('homes', '0002_article_image'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='article',
            name='public',
        ),
        migrations.AddField(
            model_name='article',
            name='private',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
