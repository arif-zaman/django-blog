from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect
from django.template import RequestContext
from django.contrib import auth
from django.contrib.auth.hashers import make_password
from django.core.context_processors import csrf
from profiles.models import Profile
from django.contrib.auth.models import User


def login(request):
    if 'name' in request.session:
        name = request.session['name']
        return HttpResponseRedirect("/testblog/home/")

    return render_to_response("login.html", context_instance=RequestContext(request))


def verify_login(request):
    if 'name' in request.session:
        name = request.session['name']
        return HttpResponseRedirect("/testblog/home/")

    email = request.POST.get("email", "")
    password = request.POST.get("password", "")

    try:
    	user = User.objects.get(email=email)
    	if user.check_password(password):
    		request.session['name'] = user.username
        	return HttpResponseRedirect("/testblog/home/")
        else:
        	return HttpResponseRedirect("/testblog/invalid/")

    except:
    	return HttpResponseRedirect("/testblog/invalid/")


def logout(request):
	if 'name' in request.session:
		auth.logout(request)
		return HttpResponseRedirect("/testblog/home/")

	return HttpResponseRedirect('/testblog/login/')


def invalid_login(request):
    if 'name' in request.session:
        name = request.session['name']
        return HttpResponseRedirect("/testblog/home/")

    return render_to_response("invalid_login.html")


def signup(request):

	if 'name' in request.session:
		name = request.session['name']
		address = "/testblog/home/"
		return HttpResponseRedirect(address)
	
	if request.method == 'POST':
		uname = request.POST.get("username", "")
		email = request.POST.get("email", "")
		passwd1 = request.POST.get("password1", "")
		passwd2 = request.POST.get("password2", "")
		old_user = User.objects.filter(email=email)

		if passwd1 != passwd2:
			passwd_alert = True

		else:
			if old_user:
				email_alert = True
			
			else:
				try:
					new_user = User(username=uname,email=email,password=make_password(passwd1))
					new_user.save()
					new_profile = Profile(username=uname,email=email)
					new_profile.save()
					return HttpResponseRedirect('/testblog/signup_success')
				except:
					username_alert = True

	return render_to_response("signup.html",locals(), context_instance=RequestContext(request))

def signup_success(request):

	return render_to_response("signup_success.html")